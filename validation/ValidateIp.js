const Joi = require('joi');

const ipSchema = Joi.object({
    ip: Joi.string().ip().required(),
    coordinates: Joi.object().keys({
        x: Joi.number().required(),
        y: Joi.number().required()
    }).required()
});

module.exports= ipSchema;