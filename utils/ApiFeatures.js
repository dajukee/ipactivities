
const distance=(x1, x2, y1, y2)=>{
    return Math.sqrt((x2-x1)^2 + (y2-y1)^2)
}


// creating a class for the api features like query filtering
class ApiFeatures{
    constructor(Model, query){
        this.Model= Model,
        this.query= query
    }
    filter(){
        const queryObj= {...this.query}
        if(this.Model && queryObj && Object.keys(queryObj).length !== 0){
            const SimilarIps= this.Model.filter((curr)=> curr.ip === queryObj["ip"] && (Date.now() - curr.created_at) < 60 * 60 * 1000);

            let x1= SimilarIps.length!==0 && SimilarIps[0].coordinates.x;
            let y1= SimilarIps.length!==0 && SimilarIps[0].coordinates.y;
            let dist = 0;
            SimilarIps.forEach((curr) => {
                dist += distance(x1, curr.coordinates.x, y1, curr.coordinates.y)
                x1= curr.coordinates.x;
                y1= curr.coordinates.y;
            });
            this.Model= {distance: dist};
        }
        return this.Model;
    }
}

module.exports= ApiFeatures;