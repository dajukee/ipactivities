const joiIpSchema= require('../validation/ValidateIp');
const { ClientError } = require('./AppError');


module.exports= (fn) =>{
    return(req, res, next)=>{
        const validation = joiIpSchema.validate(req.body);
        console.log(validation)
        if(!validation.error){
            fn(req, res, next).catch(next)
        }else{
            return next(new ClientError(`request body is not valid ${validation.error}`, 422))
        }
    }
}