
class AppError extends Error{
    constructor(message, statusCode){
        super(message)
        this.statusCode= statusCode;
        this.status= `${statusCode}`.startsWith('4') ? 'fail': 'error';
        this.isOperational= true;
        Error.captureStackTrace(this, this.constructor);
    }
}

class ClientError extends AppError{
    constructor(message, statusCode){
        super(message, statusCode)
    }
}

class ServerError extends AppError{
    constructor(message, statusCode){
        super(message, 500)
    }
}


module.exports={
    AppError,
    ClientError,
    ServerError
}