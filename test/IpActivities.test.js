
const app = require('../app');
const request = require("supertest");

jest.setTimeout(500000)

let server = request(app)


describe("ip-activities-routes", () => {
  it("GET /ipActiivities - success", async () => {
    const res = await server.get("/analytics");
    expect(res.status).toEqual(200);
  });
});

describe("ip-activities-routes", () => {
  it("POST /ipActivities - success", async () => {
    const res = await server.post("/analytics").send({ip:'10.199.212.2', coordinates:{ x: 30.5, y: 15.255 }});
    expect(res.status).toEqual(201);
  });
});

describe("ip-activities-routes", () => {
  it("GET /ipActivities?ip=query - success", async () => {
    const res = await server.get("/analytics?ip=10.199.212.2");
    expect(res.status).toEqual(200);
  });
});
