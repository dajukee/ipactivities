const fs= require('fs');

// simulate post and get request



const readFile= ()=>{
    return fs.readFileSync(`${__dirname}/db.json`, 'utf-8')
}

const writeFile= (data)=>{
    return fs.writeFileSync(`${__dirname}/db.json`, JSON.stringify(data)) 
}

class DbMethods{
    static create(data){
        let readData= readFile();
        if(!readData){
            var Ips=  []
            data.created_at= Date.now();
            Ips.push(data)
            writeFile(Ips)
        }else{
            let fetchedIps= JSON.parse(readData);
            data.created_at= Date.now();
            fetchedIps.push(data)
            writeFile(fetchedIps)
        }
        return data
    }

    static find(){
        let AllData= null;
        let readData= fs.readFileSync(`${__dirname}/db.json`, 'utf-8');
        if(readData){
            AllData= JSON.parse(readData);
        }
        return AllData;
    }


    static findOne(data, query){
        let singleData= null;
        let readData= fs.readFileSync(`${__dirname}/db.json`, 'utf-8');
        if(readData){
            const DBData= JSON.parse(readData);
            singleData= DBData.find((curr)=> curr.ip === data.ip && curr.coordinates === data.coordinates)
        }
        return singleData
    }
}

module.exports= DbMethods;

