const router = require('express').Router()
const IP = require('../controllers/ipActivities')

router.get('/analytics', IP.FetchActivityForIP)

router.post('/analytics', IP.StoreActivityForIP)



module.exports= router