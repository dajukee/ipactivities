const fs= require('fs');
const path=require('path');
const express= require('express');
const ipRoutes= require(`${__dirname}/routes/ipRoutes`)
const morgan= require('morgan')
const dotenv= require('dotenv')
const GlobalErrorHandler= require(`${__dirname}/controllers/errorController`)


dotenv.config()

const app= express();


app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader(
        'Access-Control-Allow-Methods',
        'GET, POST, OPTIONS, PUT, PATCH, DELETE'
    );
    res.setHeader(
        'Access-Control-Allow-Headers',
        'Authorization, Origin, Content-Type, Accept'
    );
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), { flags: 'a' })

app.use(morgan('combined', { stream: accessLogStream }))
app.use(express.json({limit: '10kb'}));
app.use('/', ipRoutes)

app.use(GlobalErrorHandler)


module.exports= app;