const Joi= require('joi');
const DbMethods= require('../DB/dbUtilities')
const ApiFeatures= require('../utils/ApiFeatures')
const Validator= require('../utils/Validator');

exports.FetchActivityForIP = (req, res, next)=>{

    const dataFetched = new ApiFeatures(DbMethods.find(), req.query).filter()

    res.status(200).json({
        status: 'success',
        result: dataFetched ? dataFetched.length: 0,
        data: dataFetched
    })
}



exports.StoreActivityForIP = Validator(async (req, res, next)=>{
    let dataStored= await DbMethods.create(req.body);

    res.status(201).json({
        status: 'success',
        data: dataStored
    })
})

